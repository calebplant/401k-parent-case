@isTest
public inherited sharing class CaseTests {

    private static String PARENT_CASE_SUBJECT = 'parentCase';

    @TestSetup
    static void makeData(){
        Case parentCase = new Case(Subject=PARENT_CASE_SUBJECT);
        insert parentCase;
    }

    @isTest
    static void doesPreventDirectParentClose() {
        Case parentCase = [SELECT Id, Status, Allow_Delete__c FROM Case WHERE Subject = :PARENT_CASE_SUBJECT LIMIT 1];
        System.debug(parentCase);

        Case child1 = new Case(Subject='child1', ParentId=parentCase.Id);
        insert child1;
        Case child2 = new Case(Subject='child2', ParentId=parentCase.Id);
        insert child2;

        Test.startTest();
        try {
            child1.Status = 'Closed';
            update child1;
            parentCase.Status = 'Closed';
            update parentCase;
        } catch(Exception e) {
            System.debug(e.getMessage());
            Test.stopTest();
        }


        List<Case> finalCases = [SELECT Id, Subject, Status, Prevent_Save__c, Allow_Delete__c FROM Case];
        for(Case eachCase : finalCases) {
            System.debug(eachCase);
        }

        parentCase = [SELECT Id, Subject, Status FROM Case WHERE Subject = :PARENT_CASE_SUBJECT LIMIT 1];
        System.assertNotEquals('Closed', parentCase.Status);
    }

    @isTest
    static void doesCloseParentCaseWhenChildrenClosed() {
        Case parentCase = [SELECT Id, Status, Allow_Delete__c FROM Case WHERE Subject = :PARENT_CASE_SUBJECT LIMIT 1];
        System.debug(parentCase);

        Case child1 = new Case(Subject='child1', ParentId=parentCase.Id);
        insert child1;
        Case child2 = new Case(Subject='child2', ParentId=parentCase.Id);
        insert child2;

        Test.startTest();
        try {
            child1.Status = 'Closed';
            update child1;
            child2.Status = 'Closed';
            update child2;
        } catch(Exception e) {
            System.debug(e.getMessage());
            Test.stopTest();
        }


        List<Case> finalCases = [SELECT Id, Subject, Status, Prevent_Save__c, Allow_Delete__c FROM Case];
        for(Case eachCase : finalCases) {
            System.debug(eachCase);
        }

        parentCase = [SELECT Id, Subject, Status FROM Case WHERE Subject = :PARENT_CASE_SUBJECT LIMIT 1];
        System.assertEquals('Closed', parentCase.Status);
    }

    @isTest
    static void doesCloseCaseWithNoParentOrChildren() {
        Case simpleCase = new Case(Subject = 'simpleCase');
        insert simpleCase;

        Test.startTest();
        simpleCase.Status = 'Closed';
        update simpleCase;
        Test.stopTest();

        simpleCase = [SELECT Id, Status FROM Case WHERE Subject = 'simpleCase' LIMIT 1];
        System.assertEquals('Closed', simpleCase.Status);
    }
}
