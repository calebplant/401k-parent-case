@isTest
public inherited sharing class BenefitsTests {

    private static Integer STARTING_BENEFITS_SIZE = 3;

    @TestSetup
    static void makeData(){
        List<Employee__c> emps = new List<Employee__c>{
            new Employee__c(Name='emp1')
        };
        insert emps;

        List<Benefit__c> bens = new List<Benefit__c>();
        //lower bound
        bens.add(new Benefit__c(Employee__c = emps[0].Id,Type__c = '401k',
            Start_Date__c = System.today().addDays(1),End_Date__c = System.today().addDays(3)));
        bens.add(new Benefit__c(Employee__c = emps[0].Id,Type__c = 'Dental',
            Start_Date__c = System.today().addDays(1),End_Date__c = System.today().addDays(3)));
        // Upper bound
        bens.add(new Benefit__c(Employee__c = emps[0].Id,Type__c = '401k',
            Start_Date__c = System.today().addDays(6),End_Date__c = System.today().addDays(8)));
        
        insert bens;
    }
    
    @isTest
    static void doesPreventOverlappingBenefitOfSameType() {
        Id empId = [SELECT Id FROM Employee__c LIMIT 1].Id;
        Benefit__c overlappingBenefit = new Benefit__c(Employee__c = empId,Type__c = '401k',
                                        Start_Date__c = System.today().addDays(2),End_Date__c = System.today().addDays(4));
        
        Test.startTest();
        try {
            insert overlappingBenefit;
        } catch (Exception e) {
            System.debug(e.getMessage());
        } finally {
            Test.stopTest();
        }

        List<Benefit__c> testBens = [SELECT Id, Type__c, Start_Date__c, End_Date__c
                                    FROM Benefit__c
                                    WHERE Start_Date__c <= :overlappingBenefit.End_Date__c
                                    AND End_Date__c >= :overlappingBenefit.Start_Date__c
                                    AND Employee__c = :empId
                                    AND Type__c = :overlappingBenefit.Type__c];
        System.debug(testBens);

        List<Benefit__c> finalBens = [SELECT Id FROM Benefit__c];
        System.assert(finalBens.size() == STARTING_BENEFITS_SIZE);
    }

    @isTest
    static void doesAllowOverlappingBenefitOfDifferentType() {
        Id empId = [SELECT Id FROM Employee__c LIMIT 1].Id;
        Benefit__c overlappingBenefit = new Benefit__c(Employee__c = empId,Type__c = 'Health',
                                        Start_Date__c = System.today().addDays(2),End_Date__c = System.today().addDays(4));
        
        Test.startTest();
        try {
            insert overlappingBenefit;
        } catch (Exception e) {
            System.debug(e.getMessage());
        } finally {
            Test.stopTest();
        }

        List<Benefit__c> finalBens = [SELECT Id FROM Benefit__c];
        System.assert(finalBens.size() == STARTING_BENEFITS_SIZE + 1);
    }
}
