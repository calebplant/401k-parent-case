# Benefits Overlap & Parent Case Tasks

## Demo (~70 sec)

[Quick demo showing the result for both tasks](https://www.youtube.com/watch?v=CT_2XX4Md3I)

## Part 1: Benefits Overlap

### Overview

A record-triggered flow preventing benefit records from being created if a record already exists of the same type that has overlapping dates with the new record.

### Flows

#### Before-Save Record-Triggered Flow

![Before-Save Record-Triggered Flow](media/flow-benefits.png)

The flow simply takes the new benefit's Employee Id and queries for benefit records that would violate our rule (no date overlap of same type). If any are found, it writes to a field that will cause a validation rule to fail.

## Part 2: Parent Case Closure

### Overview

Two record-triggered flows that prevent a Parent Case from being directly closed by a user. They also automatically close a Parent Case once all its children are closed.

### Flows

#### Before-Update Record-Triggered Flow

![Before-Update Record-Triggered Flow](media/flow-case-before.png)

The flow launches when Status is updated to Closed, and first queries for child cases on the flow record.

* Children are found:
It checks if Allow Delete is set to true. If not, it sets Prevent Save and displays the error message for trying to directly delete a Parent Case.

* No children found:
It first checks if the flow record has a Parent Case. If not, it ends the flow. If yes, it will loop through all the Parent Case's children and check if any of their Status is not Closed. If all the child cases were closed, it sets Allow Delete to delete the Parent Case.

#### After-Update Record-Triggered Flow

![Before-Update Record-Triggered Flow](media/flow-case-after.png)

If Allow Delete was set in the before-update flow, this flow runs. It simply queries for the Parent Case, sets its Status to Closed and Allow Delete to true, sets Allow Delete on the flow record to false, then updates the flow record and Parent Case.